package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

type APIResponse struct {
	Success bool
	Data    map[string]interface{}
}

func ApiGet(path string) (*APIResponse, error) {
	resp, err := http.Get("https://library.wiese2.org/term" + path)
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var res APIResponse
	err = json.Unmarshal(b, &res)
	if err != nil {
		return nil, err
	}

	if !res.Success {
		return nil, errors.New("api says no")
	}

	return &res, nil
}
