package main

import (
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().Unix())

	Clear()

	ClearLine()
	PrintMultilineCenter(logo, 0)
	PrintlnCenter("by milan44", 0)
	ClearLine()

	StartHandler()
}
