package main

import (
	"bufio"
	"fmt"
	"github.com/fatih/color"
	"github.com/inancgumus/screen"
	"golang.org/x/crypto/ssh/terminal"
	"math/rand"
	"os"
	"strings"
	"time"
)

func Println(str string) {
	PrintMultilineCenter(LimitString(str), maxLineLength)
}

func ClearLine() {
	fmt.Println("                                             ")
}

func Clear() {
	screen.Clear()
	screen.MoveTopLeft()
}

func LimitString(str string) string {
	runes := []rune(str)

	if len(runes) < maxLineLength {
		return str
	}

	var lines []string
	for len(runes) > 0 {
		if len(runes) < maxLineLength {
			lines = append(lines, string(runes))
			break
		}

		lines = append(lines, string(runes[:maxLineLength]))
		runes = runes[maxLineLength:]
	}

	return strings.Join(lines, "\n")
}

func PrintMultilineCenter(str string, maxLineSize int) {
	lines := strings.Split(str, "\n")

	if maxLineSize == 0 {
		for _, line := range lines {
			if len([]rune(line)) > maxLineSize {
				maxLineSize = len([]rune(line))
			}
		}
	}

	for _, line := range lines {
		PrintlnCenter(line, maxLineSize)
	}
}

func PrintlnCenter(line string, maxLineSize int) {
	if maxLineSize == 0 {
		maxLineSize = len([]rune(line))
	}
	PrintCenter(line+"\n", maxLineSize)
}

func PrintList(list []string) {
	max := 0
	for x, item := range list {
		if len([]rune(item)) > max {
			max = len([]rune(item))
		}

		if len([]rune(item)) >= maxLineLength {
			list[x] = string([]rune(item)[:maxLineLength-4]) + "..."
		}
	}

	lines := list
	if max < maxLineLength/2 {
		lines = make([]string, 0)
		line := ""
		for i := 0; i < len(list); i++ {
			nLine := PadRight(list[i], max+3)
			if len([]rune(line+nLine)) > maxLineLength {
				lines = append(lines, line)
				line = nLine
			} else {
				line += nLine
			}
		}

		if line != "" {
			lines = append(lines, line)
		}
	}
	for _, line := range lines {
		PrintlnCenter(strings.TrimSpace(line), maxLineLength)
	}
}

func PrintPage(page string) {
	words := strings.Split(page, " ")

	lines := make([]string, 0)
	for _, word := range words {
		if len([]rune(word)) > maxLineLength {
			for len([]rune(word)) > maxLineLength {
				lines = append(lines, string([]rune(word)[:maxLineLength]))
				word = string([]rune(word)[maxLineLength:])
			}
			lines = append(lines, word)
		} else {
			lastX := len(lines) - 1
			if lastX < 0 {
				lines = append(lines, word)
			} else {
				newLine := lines[lastX] + " " + word
				if len([]rune(newLine)) > maxLineLength {
					lines = append(lines, word)
				} else {
					lines[lastX] = newLine
				}
			}
		}
	}

	for _, line := range lines {
		PrintlnCenter(strings.TrimSpace(line), maxLineLength)
	}
}

func PrintCenter(line string, maxLineSize int) {
	width, _, _ := terminal.GetSize(int(os.Stdout.Fd()))

	if maxLineSize == 0 {
		maxLineSize = len([]rune(line))
	}

	leftPadding := (width - maxLineSize) / 2
	c := color.New(color.FgYellow)

	line = strings.Repeat(" ", leftPadding) + line

	for _, ch := range []rune(line) {
		if ch != ' ' {
			ms := rand.Intn(10) + 5
			time.Sleep(time.Duration(ms) * time.Millisecond)
		}
		_, _ = c.Print(string(ch))
	}
}

func Input() string {
	y := color.New(color.FgYellow)
	hy := color.New(color.FgHiYellow)
	
	PrintCenter("", maxLineLength)
	y.Print(pwd.String())
	hy.Print(" > ")
	
	reader := bufio.NewReader(os.Stdin)

	color.Set(color.FgYellow)
	text, _ := reader.ReadString('\n')
	color.Unset()

	return strings.TrimSpace(text)
}

func PadRight(str string, l int) string {
	l -= len([]rune(str))
	if l < 0 {
		l = 0
	}
	return str + strings.Repeat(" ", l)
}
