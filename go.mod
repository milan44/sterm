module main

go 1.14

require (
	github.com/fatih/color v1.10.0
	github.com/go-ole/go-ole v1.2.4
	github.com/inancgumus/screen v0.0.0-20190314163918-06e984b86ed3
	golang.org/x/crypto v0.0.0-20201117144127-c1f2f97bffc9
)
