package main

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type Command struct {
	Command string
	Args    []string
}

type Location struct {
	Wall  string
	Shelf string
	Book  string
}

var pwd Location

var aliases = map[string]string {
	"?": "help",
	"h": "help",
	"show": "view",
	"x": "exit",
	"quit": "exit",
	"ls": "list",
	"pwd": "where",
}

func StartHandler() {
	var cmd Command
	for cmd.Command != "exit" {
		cmd = ParseCommand(Input())

		switch cmd.Command {
		case "where":
			pwd.Print()
			ClearLine()
		case "goto":
			pwd.ChangeDir(strings.Join(cmd.Args, " "))
			ClearLine()
		case "list":
			pwd.ListFiles()
			ClearLine()
		case "view":
			pwd.ViewPage(cmd.Args[0])
			ClearLine()
		case "next":
			pwd.NextPage()
			ClearLine()
		case "prev":
			pwd.PrevPage()
			ClearLine()
		case "rand":
			pwd.Random()
			ClearLine()
		case "help":
			Println("--- Help Page ---")
			Println("where        Shows the current path")
			Println("goto [dir]   Moves path into a directory (.. to go up, / to go to root)")
			Println("list         List all files in the current directory")
			Println("view [page]  Views the page [page]")
			Println("next         Views the next page")
			Println("prev         Views the previous page")
			Println("rand         Goes to a random book and views a random page")
			Println("help         Shows this help")
			Println("exit         Exits Sterm")
			ClearLine()
		case "exit":
			Println("Goodbye!")
		case "":
			continue
		default:
			Println("Error: Command '" + cmd.Command + "' not found")
			ClearLine()
		}
	}
}

func ParseCommand(cmd string) Command {
	splt := strings.Split(strings.TrimSpace(cmd), " ")

	c := Command{
		Command: splt[0],
	}
	
	if aliases[c.Command] != "" {
		c.Command = aliases[c.Command]
	}

	if len(splt) > 1 {
		c.Args = splt[1:]
	} else {
		c.Args = []string{""}
	}

	return c
}

func (l Location) String() string {
	loc := "/"
	if l.Wall != "" {
		loc += "wall_" + l.Wall + "/"

		if l.Shelf != "" {
			loc += "shelf_" + l.Shelf + "/"

			if l.Book != "" {
				book := l.Book
				if len([]rune(book)) > 15 {
					book = string([]rune(book)[:12]) + "..."
				}
				loc += book + "/"
			}
		}
	}
	return loc
}

func (l Location) Print() {
	Println(l.String())
}

func (l Location) GetParams() url.Values {
	path := url.Values{}

	if l.Wall != "" {
		path["wall"] = []string{l.Wall}

		if l.Shelf != "" {
			path["shelf"] = []string{l.Shelf}

			if l.Book != "" {
				path["book"] = []string{l.Book}
			}
		}
	}

	return path
}

func (l Location) ListFiles() {
	resp, err := ApiGet("/list.php?" + l.GetParams().Encode())
	if err != nil {
		Println(generalCmdError)
		return
	}

	isValid := resp.Data["hasFiles"].(bool)
	if isValid {
		pwd.Book = resp.Data["book"].(string)

		PrintList(castSlice(resp.Data["files"].([]interface{})))
		return
	}

	Println("Error: Invalid path")
}

func (l *Location) ValidatePath(path url.Values) bool {
	resp, err := ApiGet("/list.php?" + path.Encode())
	if err != nil {
		return false
	}

	isValid := resp.Data["hasFiles"].(bool)
	if isValid {
		l.Book = resp.Data["book"].(string)
		
		return true
	}
	return false
}

func (l Location) Random() {
	resp, err := ApiGet("/rand.php")
	if err != nil {
		Println(generalCmdError)
		return
	}
	
	newLoc := Location{
		Wall: resp.Data["wall"].(string),
		Shelf: resp.Data["shelf"].(string),
		Book: resp.Data["book"].(string),
	}
	
	if newLoc.ValidatePath(newLoc.GetParams()) {
		pwd = newLoc
		previousPage = 0
		
		pwd.ViewPage(resp.Data["page"].(string))
		return
	}
	
	Println(generalCmdError)
}

func (l Location) ChangeDir(s string) {
	path := url.Values{}
	newLoc := pwd

	if s == "." {
		Println("Doing nothing")
		return
	}

	if s == "/" {
		if l.Wall == "" {
			Println("Already at root")
			return
		}
	
		newLoc.Wall = ""
		newLoc.Shelf = ""
		newLoc.Book = ""
		
		previousPage = 0
		Println("Entered Root")
		return
	}
	if s == ".." {
		if l.Book != "" {
			newLoc.Book = ""
			Println("Entered Shelf " + l.Shelf)
		} else if l.Shelf != "" {
			newLoc.Shelf = ""
			Println("Entered Wall " + l.Wall)
		} else if l.Wall != "" {
			newLoc.Wall = ""
			Println("Entered Root")
		} else {
			Println("Already at root")
			return
		}

		previousPage = 0
		pwd = newLoc
		return
	}

	msg := "Doing nothing"
	if l.Wall == "" {
		path["wall"] = []string{s}
		newLoc.Wall = s
		msg = "Entered Wall " + s
	} else {
		path["wall"] = []string{l.Wall}

		if l.Shelf == "" {
			path["shelf"] = []string{s}
			newLoc.Shelf = s
			msg = "Entered Shelf " + s
		} else {
			path["shelf"] = []string{l.Shelf}

			if l.Book == "" {
				path["book"] = []string{s}
				newLoc.Book = s
				msg = "Entered Book " + s
			} else {
				Println("Error: Already reached maximum depth")
				return
			}
		}
	}

	if newLoc.ValidatePath(path) {
		pwd = newLoc
		previousPage = 0
		Println(msg)
		return
	}

	Println("Error: Invalid path")
}

var previousPage = 0

func (l Location) NextPage() {
	if previousPage == 0 {
		Println("You haven't viewed any page")
		return
	}
	
	previousPage++
	if previousPage > 64 {
		previousPage--
		Println("You have viewed the last page")
		return
	}
	
	pwd.ViewPage("")
}

func (l Location) PrevPage() {
	if previousPage == 0 {
		Println("You haven't viewed any page")
		return
	}
	
	previousPage--
	if previousPage < 1 {
		previousPage++
		Println("You have viewed the first page")
		return
	}
	
	pwd.ViewPage("")
}

func (l Location) ViewPage(s string) {
	if s == "" && previousPage != 0 {
		s = strconv.Itoa(previousPage)
	}

	page, err := strconv.Atoi(s)
	if err != nil || page < 1 || page > 64 {
		Println("Error: Invalid page (1 to 64)")
		return
	}
	
	previousPage = page

	path := "https://library.wiese2.org/data/wall_" + url.PathEscape(l.Wall) + "/shelf_" + l.Shelf + "/" + url.PathEscape(l.Book) + "/page_" + s + ".txt"
	resp, err := http.Get(path)
	if err != nil {
		Println("Error: Failed to load page")
		return
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		Println("Error: Failed to load page")
		return
	}
	
	spl := strings.Split(l.Book, "_")
	title := spl[len(spl) - 1]
	
	if len([]rune(title)) > maxLineLength-8 {
		title = string([]rune(title)[:maxLineLength-11]) + "..."
	}

	PrintlnCenter("--- " + title + " ---", 0)
	PrintlnCenter("Wall " + l.Wall + ", Shelf " + l.Shelf + ", Book " + spl[len(spl) - 2] + ", Page " + s, 0)
	Println("-")
	PrintPage(string(b))
	Println("-")
}

func castSlice(sl []interface{}) []string {
	nsl := make([]string, len(sl))
	for k, v := range sl {
		nsl[k] = v.(string)
	}

	return nsl
}
