package main

const (
	logo = `████ █████ ████ ████ ██ ██
█      █   █    █  █ █ █ █
████   █   ████ ████ █ █ █
   █   █   █    █ █  █ █ █
████   █   ████ █  █ █   █`
	maxLineLength    = 100
	defaultInputLine = "> "
	generalCmdError  = "Error: Failed to execute command"
)
